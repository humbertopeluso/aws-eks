provider "aws" {
  access_key = var.aws_access_key
  secret_key = var.aws_secret_key
  region     = var.aws_region
}

module "cluster" {
  source  = "gitlab.com/vkpr/terraform-aws-eks/aws"
  version = "~> 1.3.0"

  cluster_name      = local.config.cluster_name
  cluster_version   = local.config.cluster_version
  cidr_block        = local.config.cidr_block
  private_subnets   = local.config.private_subnets
  public_subnets    = local.config.public_subnets
  node_groups       = local.config.node_groups
  tags              = local.config.tags
  create_kubeconfig = true
}
